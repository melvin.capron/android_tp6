package com.example.todocapron;

import android.app.Activity;
import android.content.Intent;
import android.os.Bundle;

import com.google.android.material.floatingactionbutton.FloatingActionButton;

import androidx.appcompat.app.AppCompatActivity;
import androidx.appcompat.widget.Toolbar;

import android.util.SparseBooleanArray;
import android.view.View;

import android.view.Menu;
import android.view.MenuItem;
import android.widget.ArrayAdapter;
import android.widget.Button;
import android.widget.ListView;

import java.io.BufferedReader;
import java.io.File;
import java.io.FileReader;
import java.io.FileWriter;
import java.io.IOException;
import java.util.ArrayList;

public class MainActivity extends AppCompatActivity {
    final int LAUNCH_ADD_LIST_ACTIVITY = 1;
    final String FILE_TO_SAVE = "todos.txt";

    ArrayList<String> elements = new ArrayList<>();

    ArrayAdapter<String> adapter;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        //read all tasks
        this.readTasks();

        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
        Toolbar toolbar = findViewById(R.id.toolbar);
        setSupportActionBar(toolbar);

        //Init floating button add todo
        FloatingActionButton fab = findViewById(R.id.fab);

        fab.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                Intent addList = new Intent(getApplicationContext(), AddListActivity.class);

                startActivityForResult(addList, LAUNCH_ADD_LIST_ACTIVITY);
            }
        });

        //Init remove selected tasks button
        Button removeSelectedTasks = findViewById(R.id.removeSelected);

        removeSelectedTasks.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                System.out.println("clicked");
                removeTasks();
            }
        });

        //Init save tasks button
        Button saveList = findViewById(R.id.saveList);

        saveList.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                try {
                    saveTasks();
                } catch (IOException e) {
                    e.printStackTrace();
                }
            }
        });

        //creating the adapter
        this.adapter = new ArrayAdapter<>(MainActivity.this, android.R.layout.simple_list_item_checked, elements);

        //setting the adapter
        ListView lv = (ListView) findViewById(R.id.todos);
        lv.setAdapter(adapter);
    }

    /**
     * @param requestCode given request code when the activity is launched to know who answer us
     * @param resultCode  the result code
     * @param data        the data given by the intent
     */
    protected void onActivityResult(int requestCode, int resultCode, Intent data) {
        // when there is a result for one activity, handle it
        super.onActivityResult(requestCode, resultCode, data);

        if (resultCode == Activity.RESULT_OK) {
            switch (requestCode) {
                case LAUNCH_ADD_LIST_ACTIVITY:
                    //add data to from activity to list
                    data.getStringExtra("todo");
                    elements.add(data.getStringExtra("todo"));
                    adapter.notifyDataSetChanged();
                    break;
                default:
                    //otherwise, we can't proceed the query
                    System.out.println("The given request code has not been processed.");
                    return;
            }
        }
    }

    private void saveTasks() throws IOException {
        // getting file
        final File myFile = new File(MainActivity.this.getFilesDir(), FILE_TO_SAVE);

        // create non existing
        if (!myFile.exists()) {
            myFile.createNewFile();
        }
        // write to file
        FileWriter writer = new FileWriter(myFile);
        for (String str : this.elements) {
            //escape new line for more readability
            writer.write(str + '\n');
        }
        // close file
        writer.close();
    }

    private void readTasks() {
        // load file
        final File myFile = new File(MainActivity.this.getFilesDir(), FILE_TO_SAVE);

        try {
            // open buffer
            BufferedReader br = new BufferedReader(new FileReader(myFile));
            String line;

            // read file and checking for a new line
            while ((line = br.readLine()) != null) {
                // looping over
                this.elements.add(line);
            }
            // close
            br.close();
        } catch (IOException e) {
            e.printStackTrace();
        }
    }

    private void removeTasks() {
        ListView lv = (ListView) findViewById(R.id.todos);
        SparseBooleanArray checked = lv.getCheckedItemPositions();

        // all tasks to remove
        ArrayList<String> tasksToRemove = new ArrayList<>();

        // checking all list
        for (int i = 0; i < lv.getAdapter().getCount(); i++) {
            // if checked
            if (checked.get(i)) {
                // add to array list
                tasksToRemove.add(this.elements.get(i));
            }
        }
        // delete all tasks
        this.elements.removeAll(tasksToRemove);
        //remove selected
        lv.clearChoices();
        //update listviewer
        this.adapter.notifyDataSetChanged();
    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        // Inflate the menu; this adds items to the action bar if it is present.
        getMenuInflater().inflate(R.menu.menu_main, menu);
        return true;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        // Handle action bar item clicks here. The action bar will
        // automatically handle clicks on the Home/Up button, so long
        // as you specify a parent activity in AndroidManifest.xml.
        int id = item.getItemId();

        //noinspection SimplifiableIfStatement
        if (id == R.id.action_settings) {
            return true;
        }

        return super.onOptionsItemSelected(item);
    }
}
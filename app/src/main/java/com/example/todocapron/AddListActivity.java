package com.example.todocapron;

import android.content.Intent;
import android.os.Bundle;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.widget.Button;
import android.widget.TextView;

import androidx.appcompat.app.AppCompatActivity;
import androidx.appcompat.widget.Toolbar;

import com.google.android.material.floatingactionbutton.FloatingActionButton;
import com.google.android.material.snackbar.Snackbar;

import java.util.Map;

public class AddListActivity extends AppCompatActivity {

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.add_list_activity);

        //Init back button
        Button back = findViewById(R.id.back);

        back.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                Intent displayText = new Intent(getApplicationContext(), MainActivity.class);
                //cancel
                setResult(RESULT_CANCELED, displayText);
                finish();
            }
        });

        //Init send button
        Button sendTodo = findViewById(R.id.sendText);
        final TextView todo = (TextView) findViewById(R.id.textToSend);

        sendTodo.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                Intent sendTodo = new Intent(getApplicationContext(), MainActivity.class);

                //Put new data
                sendTodo.putExtra("todo", todo.getText().toString());

                //End and send ok
                setResult(RESULT_OK, sendTodo);
                finish();
            }
        });
    }
}